/**
* @file output.cpp
* @author �a�la �elik (152120131104)
* @date Dec 9, 2020
* @brief Source file of output class.
*
*	This file has method implementations for the output class.
*/

#include "output.h"
#include "tank.h"
#include "tanklist.h"
#include <iostream>
#include <fstream>
#include "engine.h"
#include "input.h"

using namespace std;
Output::Output(string filename)
{
	this->filename = filename;
}
void Output::open()
{
	file.open(filename, ios::out | ios::app);
	if (!file.is_open())
	{
		cout << "Error opening output file <" << filename << ">" << endl;
	}
}

void Output::close()
{
	file.close();
}

void Output::writeDatas(string str)
{
	file << str << endl;
}
void Output::setFilename(string filename)
{
	this->filename = filename;
}

Output output("output.txt");
