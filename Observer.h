/**
* @file Observer.h
* @author �ulenur ��rez (152120181038)
* @date 15.01.2021
* @brief Header of Observer class.
*
*	This file includes all the declarations functions for Observer interface.
*/

#pragma once
#include <string>

using namespace std;

class Observer
{
public:

    /**
     * Update the state of this observer
     * @param message is message from the subject
     */
    virtual void update(string message) = 0;
};

