/**
* @file main.cpp
* @author �a�la �elik (152120131104)
* @date Dec 9, 2020
* @brief Source file of main class.
*
*	This file is for testing purposes. It contains method. Inside the main method all the functions and features are tested.
*/


#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <string.h>
#include "TankList.h"
#include "engine.h"
#include "input.h"
#include "output.h"

using namespace std;

int main(int argc, char* argv[]) /// files taking from command line
{
	/*string inputFilename = argv[1];
	string outputFilename = argv[2];*/
	string inputFilename = "";
	string outputFilename = "";
	cin >> inputFilename;
	cin >> outputFilename;
	Input inputs;
	output.setFilename(outputFilename);
	output.open();
	inputs.readInputs(inputFilename);
}

