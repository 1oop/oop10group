/**
* @file engine.h
* @author Burak Kosova (152120171114)
* @date 09.12.2020
* @brief Header of engine class.
*
*	This file includes all the declarations of member variables and functions for engine class.
*/

#ifndef ENGINE_H_
#define ENGINE_H_
#include "tank.h"
#include "output.h"
#include <vector>
#include <sstream>
#include "Subject.h";

//!  engine class contains functions and variables related to engine.
/*!
 * engine class implemented by singleton design pattern which means only one object could be instantiated from it.
 * It implements the subject interface because the stop simulation method is in the engine class.
*/

class Engine : public Subject
{
private:
	std::vector<Observer*> observers;		//!< vector member contains the observers of the engine class(Observer design pattern)
	string message;							//!< message is to be printed from the observer classes 	

	const double fuel_per_second;			//!< this value represents the amount of fuel consumed by the engine every second.
	bool status;							//!< true if the engine is started, otherwise false.
	Tank internalTank;						//!< engine consumes fuel from its internal tank while its running.
	double totalConsumedFuel;				//!< the total amount of fuel consumed by the engine while its running.

	/** Constructor is private due to singleton design pattern.
	* sets the fuel_per_second to 5.5
	* status to true because engine is not running when its created.
	* totalConsumedFuel to 0.0
	* and internal tank capacity to 55.0
	*/
	Engine();							

	static Engine e_Instance;				//!< instance of engine
public:
	std::vector<Tank> connectedTanks;		//!< connected tanks are stored in this vector.
	
	/**	Getter of the engine since its private
	*	@return instance of the engine
	*/
	static Engine& Get() {					
		return e_Instance;
	}

	Engine(const Engine&) = delete;

	/** Lists information about all of the connected tanks via get_info() method in the tank class.
	*/
	void list_connected_tanks() const;

	/**	Getter of the fuel per second
	* @return fuel per second value
	*/
	double get_fuel_per_second() const;

	/**	Used to determine the engines status
	* @return true if the engine is started, otherwise false.
	*/
	bool get_status() const;

	/**	Starts the engine if it has at least 1 connected tank 
	*	assigns status value to true 
	*/
	void start_engine();

	/**	Gives back all of the fuel in the internal tank and stops the engine
	*	calls give_back_fuel() method
	*/
	void stop_engine();

	/**	Selects a random connected tank and absorbs fuel to engine's internal tank
	* @param amount represents the fuel quantity to be absorbed.
	*/
	void absorb_fuel(double);

	/**	Gives back fuel in the internal tank to the connected tank which has the minimum fuel in it.
	* @param amount represent the quantity to give back
	*/
	void give_back_fuel(double);

	/** Notifies all of the observer classes and stops the simulation	
	*/
	void stop_simulation();

	/**	Wait method burns fuel from the internal tank if the engine is running.
	* If internal tank doesn't have enough fuel, calls absorb_fuel() method to fill the internal tank.
	* Adds consumed value to totalConsumedFuel attribute
	* and finally after consuming fuel from internal tank if the fuel is less than 20
	* calls absorb_fuel() method to fill the internal tank
	* @param seconds represents the seconds to wait.
	*/
	void wait(int = 1);


	/**	Prints the total consumed value 
	*/
	void print_total_consumed_fuel_quantity() const;

	/**	Observers listens for any change in the message attribute and it only changes when simulation stops.
	*	This method adds an observer to the engine class since it has the stop simulation method.
	*	@param observer is pointer because the argument could be any class as long as it implements the Observer interface (Polymorphism)
	*/
	void registerObserver(Observer*) override;

	/**	Removes the given observer from the engines observers vector.
	*	@param observer is to determine the observer to remove.
	*/
	void removeObserver(Observer*) override;

	/**	Notifies the observers by calling the update() method on every observer in the engine's observers vector.
	* All of the observers have to implement the update method since they implement the Observer interface.
	*/
	void notifyObservers() override;

	/**
	 * Sets the message of the engine and calls the notifyObservers() method. 
	 * @param message is message of the engine.
	 */
	void setState(string);

};


#endif /* ENGINE_H_ */
