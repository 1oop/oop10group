/**
* @file engine.cpp
* @author Burak Kosova (152120171114)
* @date Dec 9, 2020
* @brief Source file of engine class.
*
*	This file has method implementations for the engine class.
*/

#include "engine.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

Engine Engine::e_Instance;

Engine::Engine() : fuel_per_second(5.5), status(false), totalConsumedFuel(0.0) {
	internalTank.set_capacity(55.0);		
}

void Engine::list_connected_tanks() const {
	output.open();
	if (connectedTanks.size() < 1) {	
		output.writeDatas("There isn't any connected tank!");
	}
	else {
		for (int i = 0; i < connectedTanks.size(); i++) { 
			connectedTanks[i].get_info();
		}
	}
	output.close();
}

double Engine::get_fuel_per_second() const {
	return fuel_per_second;
}

bool Engine::get_status() const {
	return status;
}

void Engine::start_engine() {
	output.open();
	if (connectedTanks.size() > 0) {
		status = true;
		output.writeDatas("Engine started!");
		absorb_fuel(internalTank.get_capacity()); // when engine started fills internal tank
	}
	else {
		output.writeDatas("Engine cannot start! There should be at least 1 connected tank to the engine");
	}
	output.close();
}

//absorb fuel method selects a random connected tank and absorbs fuel to engine's internal tank
void Engine::absorb_fuel(double amount) {
	srand(time(NULL));
	int connectedTankCount = connectedTanks.size();										// take connected tanks number
	int randomTank = rand() % connectedTankCount;										// create a random index between 0 and connected tanks number
	int count = 0; // to prevent an endless loop

	while (true) {
		if (connectedTanks[randomTank].get_fuel_quantity() >= amount) {					// if random connected tank's fuel is more than the amount
			connectedTanks[randomTank].take_fuel(amount);								// take fuel from the connected tank
			internalTank.add_fuel(amount);												// add fuel to the internal tank
			break;
		}
		else if (connectedTanks[randomTank].get_fuel_quantity() < amount) { // if random connected tank has not enough fuel in it
			int newAmount = connectedTanks[randomTank].get_fuel_quantity();  // new amount is all the fuel inside the random tank
			if (internalTank.get_capacity() - internalTank.get_fuel_quantity() >= newAmount) { // if internal tank has enough capacity
				connectedTanks[randomTank].take_fuel(newAmount);		// take all the fuel inside the connected tank
				internalTank.add_fuel(newAmount);						// add to the internal tank
			}
		}

		randomTank = rand() % connectedTankCount;
		count++;
		if (count > connectedTankCount) { // if all of the connected tanks checked and none of them have enough fuel then exit the loop
			break;
		}
	}
}

void Engine::give_back_fuel(double amount) {
	output.open();
	if (internalTank.get_fuel_quantity() > 0) {				// if internal tank has fuel in it
		double min = connectedTanks[0].get_fuel_quantity(); //represents the minimum fuel quantity
		int minIndex = 0;									// represents index of the tank which has minimum fuel in it
		for (int i = 0; i < connectedTanks.size(); i++) {
			if (connectedTanks[i].get_fuel_quantity() < min) {  // if there is another tank which has less fuel
				min = connectedTanks[i].get_fuel_quantity();   // update the minimum value
				minIndex = i;								   // set the current tank's index to minIndex variable
			}
		}
		connectedTanks[minIndex].add_fuel(amount);							
		internalTank.take_fuel(amount);									  
		
		ostringstream os;
		os << internalTank.get_fuel_quantity() << " gave back to tank: " << connectedTanks[minIndex].get_id();
		output.writeDatas(os.str());
	}
	output.close();
}

void Engine::wait(int seconds) {
	output.open();
	double fuelToConsume = seconds * fuel_per_second;
	if (status) {    
		if (fuelToConsume > internalTank.get_fuel_quantity()) { 
			absorb_fuel(internalTank.get_capacity() - internalTank.get_fuel_quantity()); 
		}

		internalTank.take_fuel(fuelToConsume);	
		totalConsumedFuel += fuelToConsume;		


		if (internalTank.get_fuel_quantity() < 20)   
			absorb_fuel(internalTank.get_capacity() - internalTank.get_fuel_quantity()); 
	}
	else {
		output.writeDatas("Engine is not running!");
	}
	output.close();
}

void Engine::print_total_consumed_fuel_quantity() const {
	ostringstream os;
	os << "Total consumed fuel: " << totalConsumedFuel;
	output.writeDatas(os.str());
}

void Engine::registerObserver(Observer* observer)
{
	observers.push_back(observer);
}

void Engine::removeObserver(Observer* observer)
{
	// find the observer
	auto iterator = std::find(observers.begin(), observers.end(), observer);

	if (iterator != observers.end()) { // observer found
		observers.erase(iterator); // remove the observer
	}
}

void Engine::notifyObservers()
{
	std::vector<Observer*>::iterator iterator = observers.begin();

	while (iterator != observers.end()) {
		(*iterator)->update(message);
		++iterator;
	}
}

void Engine::setState(string message)
{
	this->message = message;
	notifyObservers();
}

void Engine::stop_engine() {
	give_back_fuel();
	status = false;
}

void Engine::stop_simulation() {
	setState("Simulation stopped");
	exit(0);
}

