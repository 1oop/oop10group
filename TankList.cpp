/**
* @file TankList.cpp
* @author Burak Kosova (152120171114)
* @date Dec 9, 2020
* @brief Source file of TankList class.
*
*	This file has method implementations for the TankList class.
*/

#include "TankList.h"
#include "engine.h"
#include "output.h"
#include <iostream>


Engine& e = Engine::Get();

TankList::TankList() {

}

/// updates the fuel quantity values from engine's connected tank list
void TankList::update_tanks() {
	for (int i = 0; i < tanks.size(); i++) {
		for (int j = 0; j < e.connectedTanks.size(); j++) {
			if (tanks[i].get_id() == e.connectedTanks[j].get_id()) {
				tanks[i].set_fuel_quantity(e.connectedTanks[j].get_fuel_quantity());
			}
		}
	}
}

void TankList::registerTanks()
{
	for (int i = 0; i < tanks.size(); i++)
	{
		e.registerObserver(&tanks[i]);
	}
}

void TankList::add_fuel_tank(double capacity) {
	Tank t(capacity);
	tanks.push_back(t);
}

void TankList::list_fuel_tanks() {
	output.open();
	if (tanks.size() < 1) {
		output.writeDatas("There isn't any tank!");
	}
	else {
		update_tanks(); // updating the tanks before printing info
		for (int i = 0; i < tanks.size(); i++) {
			tanks[i].get_info();
		}
	}
	output.close();
}

int TankList::find_tank(int id) {       // finds the tank in the tanks vector by id and returns its index
	for (int i = 0; i < tanks.size(); i++) { // iterate through all the tanks
		if (tanks[i].get_id() == id) {   // if the id of the tank equals given id
			return i;					// return index
		}
	}
	return -1;							// return -1 if the tank doesn't exist
}

void TankList::remove_fuel_tank(int id) {
	output.open();
	int index = find_tank(id);
	ostringstream os;
	if (index != -1) {
		tanks.erase(tanks.begin() + index);                      // delete from the tanks vector
		os << "Tank id : " << id << " removed";
		output.writeDatas(os.str());
		//this for loop deletes the tank from the connected tanks vector
		for (int i = 0; i < e.connectedTanks.size(); i++) {
			if (e.connectedTanks[i].get_id() == id) {
				e.connectedTanks.erase(e.connectedTanks.begin() + i);
			}
		}
	}
	else
	{
		os << "There isn't any tank with id " << id;
		output.writeDatas(os.str());
	}
	output.close();
		
}

void TankList::break_fuel_tank(int id) {
	output.open();
	ostringstream os;
	int index = find_tank(id);
	if (index != -1)
		tanks[index].set_broken(true);
	else
	{
		os << "There isn't any tank with id " << id;
		output.writeDatas(os.str());
	}
	output.close();
		
}

void TankList::repair_fuel_tank(int id) {
	output.open();
	ostringstream os;
	int index = find_tank(id);
	if (index != -1)
		tanks[index].set_broken(false);
	else
	{
		os << "There isn't any tank with id " << id;
		output.writeDatas(os.str());
	}
	output.close();
}

void TankList::open_valve(int id) {
	output.open();
	ostringstream os;
	int index = find_tank(id);
	if (index != -1) {
		if (!tanks[index].get_valve()) {
			tanks[index].set_valve(true);
			os << "Tank " << id <<" valve's opened";
			output.writeDatas(os.str());
		}
		else
		{
			os << "Tank " << id << " valve's is already opened";
			output.writeDatas(os.str());
		}
			
	}
	else
	{
		output.writeDatas(os.str());
	}
	output.close();
}

void TankList::close_valve(int id) {
	output.open();
	ostringstream os;
	int index = find_tank(id);
	if (index != -1) {
		if (tanks[index].get_valve()) {
			tanks[index].set_valve(false);
			os << "Tank " << id << " valve's closed";
			output.writeDatas(os.str());
		}
		else
		{
			os << "Tank " << id << " valve's is already closed!";
			output.writeDatas(os.str());
		}
			
	}
	else
	{
		os << "There isn't any tank with id " << id;
		output.writeDatas(os.str());
	}
	output.close();
}

void TankList::connect_fuel_tank_to_engine(int id) {
	output.open();
	int index = find_tank(id);
	bool isAlreadyConnected = false;
	ostringstream os;
	if (index != -1) {
		for (int i = 0; i < e.connectedTanks.size(); i++) {			// iterates through the connected tanks and check whether the tank with given id is already connected
			if (e.connectedTanks[i].get_id() == tanks[index].get_id())
				isAlreadyConnected = true;
		}

		if (tanks[index].get_broken()) {
			os << "The tank " << id << " is broken cannot connect to engine!";
			output.writeDatas(os.str());
		}
		else if (!tanks[index].get_valve()) {
			os << "The tank " << id << " valve is closed cannot connect to engine!";
			output.writeDatas(os.str());
		}
		else if (isAlreadyConnected) {
			os << "The tank " << id << " is already connected to engine!";
			output.writeDatas(os.str());
		}
		else {
			e.connectedTanks.push_back(tanks[index]);
			os << "Tank id: " << tanks[index].get_id() << " connected to the engine!";
			output.writeDatas(os.str());
		}
	}
	else
	{
		os << "There isn't any tank with id " << id;
		output.writeDatas(os.str());
	}
	output.close();
}

void TankList::disconnect_fuel_tank_from_engine(int id) {
	output.open();
	int index = find_tank(id);
	bool isConnected = false;
	ostringstream os;
	if (index != -1) {
		for (int i = 0; i < e.connectedTanks.size(); i++) {			// iterates through the connected tanks and check whether the tank with given id is connected
			if (e.connectedTanks[i].get_id() == tanks[index].get_id())
				isConnected = true;
		}

		if (isConnected) {
			e.connectedTanks.erase(e.connectedTanks.begin() + index);		// deletes the tank from the connected tanks vector
			os << "Tank id: " << e.connectedTanks[index].get_id() << " disconnected from engine!";
			output.writeDatas(os.str());
		}
		else
		{
			os << "Tank id: " << id << " is not connected to engine!";
			output.writeDatas(os.str());
		}
			
	}
	else
	{
		os << "There isn't any tank with id " << id;
		output.writeDatas(os.str());
	}
	output.close();
}

void TankList::print_fuel_tank_count() const {  // could return the count or print directly
	output.open();
	ostringstream os;
	os << "There are " << tanks.size() << " tanks.";
	output.writeDatas(os.str());
	output.close();
}

void TankList::print_total_fuel_quantity() {
	output.open();
	ostringstream os;
	update_tanks();
	double total = 0.0;
	for (int i = 0; i < tanks.size(); i++) {
		total += tanks[i].get_fuel_quantity();
	}
	os << "Total fuel quantity :  " << total;
	output.writeDatas(os.str());
	output.close();
}

void TankList::print_tank_info(int id) {
	output.open();
	ostringstream os;
	update_tanks();
	int index = find_tank(id);
	if (index != -1) {
		tanks[index].get_info();

	}
	else
	{
		os << "There isn't any tank with id " << id;
		output.writeDatas(os.str());
	}
	output.close();
}

void TankList::fill_tank(int id, double quantity) {
	ostringstream os;
	output.open();

	for (int i = 0; i < tanks.size(); i++) {
		if (tanks[i].get_id() == id) {
			if (tanks[i].get_capacity() >= quantity + tanks[i].get_fuel_quantity()) {
				tanks[i].add_fuel(quantity);
				os << "Tank " << id << " is filled: " << quantity;
			}
			else
			{
				os << "Not enough capacity in the tank " << id;
				output.writeDatas(os.str());
			}
				
		}
	}

	for (int i = 0; i < e.connectedTanks.size(); i++) { // to find the tank in the connected tanks.
		if (e.connectedTanks[i].get_id() == id && e.connectedTanks[i].get_capacity() >= quantity + e.connectedTanks[i].get_fuel_quantity()) {
			e.connectedTanks[i].add_fuel(quantity);
		}
	}
	output.close();
}
