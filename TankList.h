/**
* @file TankList.h
* @author Burak Kosova (152120171114)
* @date 09.12.2020
* @brief Header of TankList class.
*
*	This file includes all the declarations of member variables and functions for TankList class.
*/

#ifndef TANKLIST_H_
#define TANKLIST_H_
#include "tank.h"
#include <vector>

//!  TankList class contains functions and variables related to tanks.
/*!
* Tanklist has a list of tanks as the name suggests and operates on the tanks.
* Operations like adding a new tank removing a specific tank and connecting the tank to engine
*/

class TankList {
private:
	std::vector<Tank> tanks; //!< The list of tanks

	/** finds the tank in the tank list by id and returns its index
	*	if the tank with the given id doesn't exist returns -1
	*	@param id is identifier of the tank
	*	@return index of the tank in the tank list
	*/
	int find_tank(int);
public:

	TankList();

	/** creates a fuel tank with the given capacity and adds it to the tank list
	*	@param capacity represents the newly created tank's capacity
	*/
	void add_fuel_tank(double = 100);

	/** iterates through the tank list and calls get_info() method on the tanks
	*/
	void list_fuel_tanks();

	/** removes the tank with the given id from tank list and if its connected to engine also from the connected tanks list  
	* @param id of the tank to be removed
	*/
	void remove_fuel_tank(int);

	/** sets the tank's broken value to true 
	* @param id of the tank to break
	*/
	void break_fuel_tank(int);

	/** sets the tank's broken value to false
	* @param id of the tank to repair
	*/
	void repair_fuel_tank(int);

	/** sets the tank's valve value to true if its not already true
	* @param id of the tank 
	*/
	void open_valve(int);

	/** sets the tank's valve value to false if its not already false
	* @param id of the tank
	*/
	void close_valve(int);

	/** Adds the tank to the engine's connected tank list if it is not already connected.
	*	Also the tank's valve has to be open
	*	tank shouldn't be broken in order to be connected to engine.
	*	@param id of the tank
	*/
	void connect_fuel_tank_to_engine(int);

	/** Removes the tank from the engine's connected tank list if it is connected.
	*	@param id of the tank
	*/
	void disconnect_fuel_tank_from_engine(int);

	/** Prints the size of the tank list
	*/
	void print_fuel_tank_count() const; 

	/** Iterates through the tank list, gets every tank's fuel quantity adds them to a variable and prints that total.
	*/
	void print_total_fuel_quantity();

	/** Finds the tank in the list and calls the get_info() method.
	* @param id of the tank
	*/
	void print_tank_info(int);

	/** Finds the tank in the list and adds the amount to it if tank has enough capacity.
	* @param id of the tank to be filled
	* @param amount represents the fuel quantity to be added.
	*/
	void fill_tank(int,double);

	/** Updates the tank list to make sure connected tanks and the tanks in the list has the same fuel quantities 
	*	since the engine absorbs fuel from connected tanks.
	*/
	void update_tanks();

	/** Registers every tank to be observer of the engine class.
	* Engine is subject because stop_simulation() method is inside the engine class.
	*/
	void registerTanks();
};

#endif /* TANKLIST_H_ */
