/**
* @file tank.h
* @author Hande Birsen (152120181107)
* @date 09.12.2020
* @brief Header of tank class.
*
*	This file has method implementations for the tank class.
*/


#ifndef TANK_H_
#define TANK_H_

#include "Observer.h"

//!  tank class contains functions and variables related to tank.
/*!
* tank class implements the Observer interface because it is an observer of the engine class
*/

class Tank : public Observer
{
private:
	static int counter;				//!< to auto increment ids
	int id;							//!< every tank has a unique id
	double capacity;				//!< represents the capacity of the tank
	bool broken;					//!< to keep track of the tank's situation
	bool valve;						//!< valve determines if tank can connect to the engine or not
	double fuel_quantity;			//!< represents how much fuel the tank currently has
public:	

	/** Constructor has default parameters capacity = 100 and fuel_quantity = 0
	* constructor increments the counter value and sets it to the current tank's id attribute
	* sets the broken and valve fields to false  
	*/
	Tank(double capacity = 100, double fuel_quantity = 0); 

	/** Getter of the id field
	* @return id
	*/
	int get_id() const;

	/** Getter of the capacity field
	* @return capacity
	*/
	double get_capacity() const;

	/** Setter of the capacity field
	* @param capacity
	*/
	void set_capacity(double);

	/** Getter of the broken field
	* @return true if the tank is broken, otherwise false
	*/
	bool get_broken() const;

	/** Setter of the broken field
	* @param value set true to break the tank and false to fix.
	*/
	void set_broken(bool);

	/** Getter of the valve field
	* @return true if the valve is open, otherwise false
	*/
	bool get_valve() const;

	/** Setter of the valve field
	* @param isOpen set true to open the valve and false to close.
	*/
	void set_valve(bool);

	/** Getter of the valve field
	* @return the fuel quantity of the tank.
	*/
	double get_fuel_quantity() const;

	/** Setter of the valve field
	* Only works if the quantity is smaller than the tank's capacity and bigger than or equal to 0.
	* @param quantity to set.
	*/
	void set_fuel_quantity(double);

	/** if the tank has enough capacity adds the amount.
	* @param amount to be added.
	*/
	void add_fuel(double);

	/** Takes fuel from the tank if the amount is smaller than the tanks current fuel quantity
	* @param amount
	*/
	void take_fuel(double);

	/** Prints all of the the tank's attributes.
	*/
	void get_info() const;

	/** Notifies the tank about engine and reacts to that change
	* @param message is tank's message to the change in the engine class
	*/
	virtual void update(string) override;
};

#endif /* TANK_H_ */
