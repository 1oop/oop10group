/**
* @file output.h
* @author �a�la �elik (152120131104)
* @date Dec 9, 2020
* @brief Header file of output class.
*
*	This file includes all the declarations of member variables and functions for output class.
*/

#ifndef OUTPUT_H_
#define OUTPUT_H_
#include <string>
#include <fstream>
using namespace std;

class Output
{
private:
	ofstream file;
	string filename;
public:
	
	/** Create output file with the given filename
	*/
	Output(string filename);
	
	/** Open the output file in append mode
	* Also check if file is open or not
	*/ 
	void open();
	
	/** Close the output file
	*/
	void close();
	
	/** Write informations on the output file 
	*/
	void writeDatas(string);
	
	/** Sets the output filename 
	*/
	void setFilename(string filename);
};
extern Output output;

#endif /* OUTPUT_H_ */
